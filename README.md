# Emco PC Mill 55 v0
This repo is to hold all the configuration and information about converting a PC Mill 55 to run using LinuxCNC

I was able to grab a PC Mill 55 and PC Turn 55 from a local makerspace looking to gain room as they had not been touched in years. 
As I found out, these machines need a special ISA RS 422 card to communicate with them, and requires Windows 98 at the latest (to be able to talk with the card)
Having not recieved the card with either machine, I was out of luck to simply get the going. I had a lead on getting a machine setup from someone on one of 
forum, had a phone call with him and seemed to have a deal worked out, and then he just seemed to drop off the face of the Earth. So I went back to my idea
of retro-fitting it with LinuxCNC. I chose to go with a MESA 7i96 ethernet card and a Raspberry Pi 4 to control it. 

At this point, I have it able to control the XYZ axis and the spindle. I need to do some tuning still on the axis's to make sure that they are moving the distance
that is being requested, as well as tuning the spindle so that when I command an RPM it is actually running that RPM.

One thing that I found is there were a couple of people out there that have done

